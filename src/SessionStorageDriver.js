import { LocalStorageDriver } from './LocalStorageDriver.js';

export class SessionStorageDriver extends LocalStorageDriver {
  constructor() {
    super();
    this.storage = window.sessionStorage;
  }
}
