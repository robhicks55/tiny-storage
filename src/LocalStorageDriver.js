export class LocalStorageDriver {
  constructor() {
    this.storage = window.localStorage;
  }

  clear() {
    return Promise.resolve(this.storage.clear());
  }

  get(key) {
    return Promise.resolve(JSON.parse(this.storage.getItem(key)));
  }

  remove(key) {
    return Promise.resolve(this.storage.removeItem(key));
  }

  set(key, value) {
    return Boolean(this.storage.setItem(key, JSON.stringify(value)));
  }
}
