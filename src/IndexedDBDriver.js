import { getIndexdDB } from './getIndexdDB.js';

export class IndexedDbDriver {

  constructor() {
    const db = getIndexdDB();
    const request = db.open(`INDEXEDDB_STORAGE:${uuid()}`, 1);
    request.onerror = () => alert('cannot proceed - you denied access to the database');
    request.onsuccess = event => this.db = event.target.result;
    this.db.onerror = event => alert('Database error:', event.target.errorCode);
    this.keys = [];
    this.objectStore = db.createObjectStore('');
  }

  clear() {
  }

  get(key) {
  }

  remove(key) {
  }

  set(key, value) {
  }
}
