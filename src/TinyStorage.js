import { getBestDriver, pickBestDriver } from './driverAvailability.js';

export class TinyStorage {
  constructor(options = {}) {
    this.dbName = options.dbName || 'foo';
    this.preferredDrivers = options.perferredDrivers || [
      'INDEXEDDB_STORAGE',
      'LOCAL_STORAGE',
      'SESSION_STORAGE'
    ];
    const bestDriverName = pickBestDriver(this.preferredDrivers);
    this.driver = getBestDriver(bestDriverName, this.dbName);
  }

  clear() {
    return this.driver.clear();
  }

  get(key) {
    return this.driver.get(key);
  }

  remove(key) {
    return this.driver.remove(key);
  }

  set(key, value) {
    return this.driver.set(key, value);
  }
}
