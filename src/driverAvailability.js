// import { CacheStorageDriver } from './CacheStorageDriver.js';
import { getIndexdDB } from './getIndexdDB.js';
import { IndexedDbDriver } from './IndexedDBDriver.js';
import { LocalStorageDriver } from './LocalStorageDriver.js';
import { SessionStorageDriver } from './SessionStorageDriver.js';

function isIndexedDbAvailable() {
  return Boolean(getIndexdDB());
}

function isLocalStorageAvailable() {
  return  window.localStorage && window.localStorage.setItem && window.localStorage.removeItem;
}

function isCachedStorageAvailable() {
  return window.CacheStorage && window.CacheStorage.has && window.CacheStorage.match && window.CacheStorage.delete;
}

function isSessionStorageAvailable() {
  return window.sessionStorage && window.sessionStorage.addItem && window.sessionStorage.getItem;
}


function isDriverSupported(driverName) {
  switch (driverName) {
    case 'INDEXEDDB_STORAGE': return isIndexedDbAvailable();
    case 'LOCAL_STORAGE': return isLocalStorageAvailable();
    // case 'CACHED_STORAGE': return isCachedStorageAvailable();
    case 'SESSION_STORAGE': return isSessionStorageAvailable();
    default: return false;
  }
}

export function pickBestDriver(preferredDrivers = []) {
  let bestDriver = null;
  for (let i = 0; i < preferredDrivers.length; i++) {
    if (isDriverSupported(preferredDrivers[i])) {
      bestDriver = preferredDrivers[i];
      break;
    }
  }
  return bestDriver;
}

export function getBestDriver(driverName) {
  if (driverName === 'INDEXEDDB_STORAGE') return new IndexedDbDriver();
  if (driverName === 'LOCAL_STORAGE') return new LocalStorageDriver();
  // if (driverName === 'CACHED_STORAGE') return new CacheStorageDriver();
  if (driverName === 'SESSION_STORAGE') return new SessionStorageDriver();
}

export const api = {
  getBestDriver,
  isDriverSupported,
  isIndexedDbAvailable,
  isLocalStorageAvailable,
  isSessionStorageAvailable
};
