import { join } from 'path';
import { terser } from 'rollup-plugin-terser';
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import liveServer from 'rollup-plugin-live-server';
import env from 'dotenv';

env.config();

const mode = process.env.NODE_ENV;
const dev = mode === 'development';
const root = process.cwd();
const input = join(root, 'src', 'TinyStorage.js');

const plugins = [ resolve(), commonjs() ];

const serverConfig = {
  input,
  plugins,
  output: {
    file: join(root, 'index.js'),
    format: 'cjs'
  }
};

const clientConfig = {
  input,
  plugins,
  output: {
    file: join(root, 'dist', 'tiny-storage.js'),
    format: 'es'
  }
};

const minClientConfig = {
  input,
  plugins: [ ...plugins, terser() ],
  output: {
    file: join(root, 'dist', 'tiny-storage.min.js'),
    format: 'es'
  }
};

const browserTestConfig = {
  input: join(root, 'test', 'tests.js'),
  plugins: [ ...plugins, liveServer({
    file: 'index.html',
    logLevel: 2,
    mount: [[ '/dist', './dist' ], [ '/src', './src' ], [ '/node_modules', './node_modules' ], [ '/test', './test' ]],
    open: false,
    port: process.env.PORT,
    root: './test',
    verbose: false,
    wait: 500
  }) ],
  output: {
    file: join(root, 'test', 'browser-test-bundle.js'),
    format: 'es'
  }
};

const serverTestConfig = {
  input: join(root, 'test', 'tests.js'),
  plugins,
  output: {
    file: join(root, 'test', 'server-test-bundle.js'),
    format: 'cjs'
  }
};

const config = !dev
  ? [ clientConfig, browserTestConfig, serverTestConfig ]
  : [ clientConfig, minClientConfig ];

export default config;
